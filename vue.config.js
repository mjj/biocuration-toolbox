const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: process.env.VUE_APP_STATIC ? '' : '/europepmc/bc-toolbox-beta/',
  // publicPath: '',

  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "epmc-patterns/foundations/index.scss";`,
      },
    },
  },

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: './src/scss/style.scss'
    }
  }
})
