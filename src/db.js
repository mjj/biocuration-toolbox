import Dexie from "dexie"

const db = new Dexie('bctDB')
db.version(1).stores({
    preferences: 'key',
    entitySets: '++id, entityType',
    filters: '++id, filterType'
})
db.version(2).stores({
    preferences: 'key',
    entitySets: '++id, entityType',
    filters: '++id, filterType',
    notations: '++idx, id',
})

db.open().catch(function (e) {
    console.error("Open failed: " + e);
});

export default db