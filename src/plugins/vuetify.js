import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                secondary: '#20699c',
                primary: '#5797c5',
                accent: '#b6d6ed',
            }
        }
    }
});
