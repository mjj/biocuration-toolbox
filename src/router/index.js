import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/MainView.vue')
  },
  {
    path: '/article/:id',
    name: 'article',
    component: () => import('../views/ArticleView.vue'),
    props: route => ({mode: 'article', id: route.params.id}),
  },
  {
    path: '/abstract/:source/:id',
    name: 'abstract',
    component: () => import('../views/ArticleView.vue'),
    props: route => ({mode: 'abstract', id: route.params.id, source: route.params.source}),
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import('../views/SignupView.vue')
  },
  {
    path: '/saved/:id?',
    name: 'saved',
    component: () => import('../views/SavedArticlesView.vue'),
    props: route => ({id: route.params.id})
  },
  {
    path: '/searches',
    name: 'searches',
    component: () => import('../views/SavedSearchesView.vue')
  },
  {
    path: '/account',
    name: 'account',
    component: () => import('../views/AccountView.vue')
  }
]

const router = new VueRouter({
  mode: process.env.VUE_APP_STATIC ? 'hash' : 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
