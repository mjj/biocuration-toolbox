var _ = require('lodash')

const OLS_ROOT = "https://www.ebi.ac.uk/ols4/api/"
const EUTILS_ROOT = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/"
const UP_ROOT = "https://rest.uniprot.org/uniprotkb/"

const jsonFetch = (path, method, body, jsonHeader=true) => {
    return fetch(path,
        {
            headers: jsonHeader ? {'Content-Type': 'application/json'} : {},
            method,
            body: body !== null ? JSON.stringify(body) : null,
            mode: 'cors'
        }).then(resp => {
            return resp.json()
        }).catch((er) => {
            return Promise.reject(er)
        })
}

export class OLS {
    static ontology_ids = {
        go: 'go',
        // snomed: 'snomed',
        uberon: 'uberon',
        // so: 'so',
        // mop: 'mop',
        efo: 'efo',
        chebi: 'chebi',
    }

    static fields = {
        iri: 'iri',
        label: 'label',
        short_form: 'short_form',
        obo_id: 'obo_id',
        ontology_name: 'ontology_name',
        ontology_prefix: 'ontology_prefix',
        description: 'description',
        type: 'type',
    }

    static async select(q, ontologies=null) {
        if (ontologies === null) {
            ontologies = Object.keys(this.ontology_ids)
        }
        return this.get(`select?q=${q}&ontology=${ontologies.join(',')}&queryFields=${Object.values(this.fields).join(',')}`)
    }

    static async get(path, body = null) {
        return this.olsFetch(path, 'GET', body)
    }

    static async olsFetch(path, method, body) {
        return jsonFetch(`${OLS_ROOT}${path}`, method, body)
    }
}

export class ncbi {

    static throttledSearchAndSummary = _.debounce(this.searchAndSummary, 1000, {trailing: true, leading: true})

    static async searchAndSummary(q, db, cb) {
        return this.search(q, db).then(results => {
            return this.summary(results.esearchresult.idlist, db).then(results => {
                return cb(results)
            })
        })
    }

    static async summary(ids, db) {
        if (!ids.length) {
            return []
        }
        return this.get(`esummary.fcgi?db=${db}&id=${ids.join(',')}&retmode=json`)
    }

    static async search(q, db) {
        return this.get(`esearch.fcgi?db=${db}&term=${q}&retmode=json`)
    }

    static async get(path, body = null) {
        return this.eutilsFetch(path, 'GET', body)
    }

    static async eutilsFetch(path, method, body) {
        return jsonFetch(`${EUTILS_ROOT}${path}`, method, body, false)
    }
}

export class UP {
    static throttledSearch = _.debounce(this.search, 200, {trailing: true, leading: true})

    static async search(q, cb) {
        return this.get(`/search?query=${q}`).then(results => cb(results))
    }

    static async get(path, body = null) {
        return this.upFetch(path, 'GET', body)
    }

    static async upFetch(path, method, body) {
        return jsonFetch(`${UP_ROOT}${path}`, method, body)
    }
}