import { fetchArticle } from '@/api/query'

export class Article {
    constructor(root) {
        this.root = root
    }

    static async loadArticle(pmcid) {
        const root = await fetchArticle(pmcid)
            .then(resp => resp.text())
            .then(body => new window.DOMParser().parseFromString(body, 'text/xml'))
            .then(xml => this.root = xml)
        return new Article(root)
    }
}