const BCT_API_ROOT = process.env.VUE_APP_BCT_API_ROOT ? process.env.VUE_APP_BCT_API_ROOT : 'http://localhost:8090/api/'
const ENTITY_SETS = 'user-filters/entity-sets'

export class BCT {
    static orcidUrl = `${BCT_API_ROOT}auth/orcid/sso`

    static async signin(username, password) {
        return this.post('auth/signin', {username, password})
    }

    static async signup(username, password, email) {
        return this.post('auth/signup', {username, password, email})
    }

    static async signout() {
        return this.post('auth/signout')
    }

    static async getUser() {
        return this.get('auth/user')
    }

    static async deleteUser() {
        return this.delete('auth/deleteUser')
    }

    static async getPrefs() {
        return this.get('preferences')
    }

    static async setPref(key, value) {
        return this.put(`preferences/${key}`, {value})
    }

    static async getEntitySets() {
        return this.get(ENTITY_SETS)//.then(resp => resp.map(e => {
        //     let setID = e[0]
        //     let content
        //     try {
        //         content = JSON.parse(e[1])
        //     } catch {
        //         content = e[1]
        //     }
        //     return [setID, content]
        // }))
    }

    static async createEntitySet(entityValue) {
        return this.post(ENTITY_SETS, {entityValue: entityValue})
    }

    static async deleteEntitySet(id) {
        return this.delete(`${ENTITY_SETS}/${id}`)
    }

    static async updateEntitySet(id, entityValue) {
        return this.put(`${ENTITY_SETS}/${id}`, {entityValue})
    }

    static async getNotations(source, externalId) {
        return this.get(`notations/article/${source}/${externalId}`)
    }

    static async createNotation(source, externalId, details) {
        return this.post(`notations/article/${source}/${externalId}`, details)
    }

    static async deleteNotation(id) {
        return this.delete(`notations/notation/${id}`)
    }

    static async updateNotation(id, details) {
        return this.put(`notations/notation/${id}`, details)
    }

    static async getCitationsWithNotes() {
        return this.get('notations/user')
    }

    static async createArticleList(name, details) {
        return this.post('lists/list', {name, details})
    }

    static async createArticleListElement(id, source, externalId, rank, details) {
        const ar_dict = {id, source, externalId}
        if (rank !== null) {
            ar_dict.rank = rank
        }
        if (details !== null) {
            ar_dict.details = details
        }
        return this.post(`lists/list/${id}`, ar_dict)
    }

    static async updateArticleList(id, name, details) {
        const up_dict = {}
        if (name !== null) {
            up_dict.name = name
        }
        if (details !== null) {
            up_dict.details = details
        }
        return this.put(`lists/list/${id}`, up_dict)
    }

    static async updateArticleListElement(id, source, externalId, rank, details) {
        const up_dict = {}
        if (rank !== null) {
            up_dict.rank = rank
        }
        if (details !== null) {
            up_dict.details = details
        }
        return this.put(`lists/list/${id}/${source}/${externalId}`, up_dict)
    }

    static async deleteArticleList(id) {
        return this.delete(`lists/list/${id}`)
    }

    static async deleteArticleListElement(id, source, externalId) {
        return this.delete(`lists/list/${id}/${source}/${externalId}`)
    }

    static async getArticleLists() {
        return this.get('lists')
    }

    static async getArticleList(id) {
        return this.get(`lists/${id}`)
    }

    static async getArticleListsByArticles(articles) {
        return this.post('lists/articles', articles)
    }

    static async rerankArticleList(id) {
        return this.post(`lists/list/${id}/rerank`)
    }

    static async createSavedSearch(parameters, name, details) {
        return this.post('searches/search', {parameters, name, details})
    }

    static async deleteSavedSearch(id) {
        return this.delete(`searches/search/${id}`)
    }

    static async updateSavedSearch(id, parameters, name, details) {
        const up_dict = {}
        if (parameters !== null) {
            up_dict.parameters = parameters
        }
        if (name !== null) {
            up_dict.name = name
        }
        if (details !== null) {
            up_dict.details = details
        }
        return this.put(`searches/search/${id}`, up_dict)
    }

    static async getSavedSearches() {
        return this.get('searches')
    }

    static async get(path, body = null) {
        return this.bctFetch(path, 'GET', body)
    }

    static async post(path, body = null) {
        return this.bctFetch(path, 'POST', body)
    }

    static async delete(path, body = null) {
        return this.bctFetch(path, 'DELETE', body)
    }

    static async put(path, body = null) {
        return this.bctFetch(path, 'PUT', body)
    }

    static async bctFetch(path, method, body) {
        return fetch(`${BCT_API_ROOT}${path}`,
            {headers: {'Content-Type': 'application/json'},
             method,
             body: body !== null ? JSON.stringify(body) : null,
             credentials: 'include'})
            .then(resp => {
                if (resp.status === 401) {
                    console.info("Logged out")
                    return Promise.reject(resp)
                } else if (!resp.ok) {
                    return Promise.reject(resp)
                }
                return resp.json()
            })
            .catch((error) => {
                return Promise.reject(error)
            })
    }
}