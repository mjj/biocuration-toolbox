export const EPMC_ROOT = 'https://europepmc.org/'
export const WS_ROOT = 'https://www.ebi.ac.uk/europepmc/'
export const EPMC_WEB_SERVICES_ROOT =
  'https://www.ebi.ac.uk/europepmc/webservices/rest/'
export const EPMC_API_ROOT = EPMC_ROOT + 'api/'
export const EPMC_API_GET_ROOT = EPMC_API_ROOT + 'get/'


const send = async (
  url,
  type = 'GET',
  payload = null,
  customParams = null,
) => {
  let params = { type }
  if (payload) {
    params.contentType = 'application/json; charset=utf-8'
    params.dataType = 'json'
    params.data = JSON.stringify(payload)
  }
  if (customParams) {
    params = { ...params, ...customParams }
  }
  return fetch(url, { params })
}

const encodeQuery = (query) => {
  return encodeURIComponent(query).replace(/%3A/g, ':').replace(/%2B/g, '+')
}
export function fetchSearchResults(params) {
  const {
    cursorMark,
    format,
    page,
    pageSize,
    query,
    ref,
    resultType,
    sort,
    synonym,
    ws,
  } = params
  let req = ws
    ? EPMC_WEB_SERVICES_ROOT + 'search'
    : EPMC_API_GET_ROOT + 'articleApi'
  req += '?query=' + (encodeQuery(query) || '')
  req += cursorMark ? '&cursorMark=' + encodeURIComponent(cursorMark) : ''
  req += '&format=' + (format || 'json')
  req += page ? '&page=' + page : ''
  req += pageSize ? '&pageSize=' + pageSize : ''
  req += ref ? '&ref=web' : ''
  req += resultType ? '&resultType=' + resultType : ''
  req += sort ? '&sort=' + sort : ''
  req += synonym ? '&synonym=' + synonym : ''
  return send(req)
}

export function fetchArticle(pmcid) {
  let req = EPMC_WEB_SERVICES_ROOT
  req += pmcid
  req += '/fullTextXML'
  return send(req)
}

export function fetchAbstract(id, source) {
  let req = EPMC_API_GET_ROOT
  req += 'articleApi?query='
  if (source != null) {
    req += `(EXT_ID:${id} AND SRC:${source})`
  } else {
    req += `(PMCID:${id})`
  }
  req += '&format=json&resultType=core'
  return send(req)
}

export function renderArticle(pmcid) {
  let req = EPMC_API_ROOT
  req += `articlerender?PMCID=${pmcid}`
  return send(req)
}

export function fetchFigures(pmcid) {
  let req = EPMC_API_GET_ROOT
  req += 'inlineImagesApi?ftid='
  req += pmcid
  return send(req)
}

export function fetchOneAnnotation(source, id, abstractOnly, provider) {
  let req = EPMC_API_GET_ROOT + 'annotationsApi?'
  req += 'source=' + source
  req += '&id=' + id
  req += abstractOnly ? '&section=Abstract' : ''
  req += provider ? '&provider=' + encodeURIComponent(provider) : ''
  return send(req)
}

export function fetchAnnotations(params) {
  const { type, subtype, ids, section, provider } = params
  let req = `${WS_ROOT}annotations_api/annotationsByArticleIds?`
  if (type) req += `type=${type}`
  if (subtype) req += `&subType=${subtype}`
  req += `&articleIds=${ids.join(',')}`
  if (section) req += `&section=${section}`
  if (provider) req += `&provider=${provider}`
  return send(req)
}

export function fetchFulltextHtml(fulltextId) {
  if (
    fulltextId.startsWith('PMC') ||
    fulltextId.startsWith('NBK') ||
    fulltextId.startsWith('mid')
  ) {
    const renderType =
      fulltextId.startsWith('PMC') || fulltextId.startsWith('mid')
        ? 'articlerender'
        : 'bookrender'
    const key =
      fulltextId.startsWith('PMC') || fulltextId.startsWith('mid')
        ? 'PMCID'
        : 'NBKID'
    if (fulltextId.startsWith('mid')) {
      fulltextId = 'mid&MANUSCRIPTID=' + fulltextId.split('/')[1]
    }
    const req = EPMC_API_ROOT + renderType + '?' + key + '=' + fulltextId
    return new Promise((resolve) => {
      send(req).then((response) => {
        let error = ''

        if (fulltextId.startsWith('PMC')) {
          if (typeof response === 'object') {
            error = response.querySelector('http-status[value="404"]')
          } else if (typeof response === 'string') {
            const div = document.createElement('div')
            div.innerHTML = response
            error =
              div.querySelector('http-status[value="404"]') ||
              div.querySelector('http-status[value="503"]')
          }
        }

        // render as preprint if it has error
        if (error) {
          renderAsPreprint(fulltextId, resolve, response)
        } else {
          resolve(response)
        }
      })
    })
  } else if (fulltextId.startsWith('PPR')) {
    return new Promise((resolve) => {
      renderAsPreprint(fulltextId, resolve)
    })
  } else if (fulltextId.startsWith('M')) {
    return new Promise((resolve) =>
      fetchFulltextXml(fulltextId, 'CTX').then((xml) =>
        transformXml(xml, 'jats2html').then((resultDocument) =>
          resolve(resultDocument)
        )
      )
    )
  }
}

export class QueryPager {
  constructor(query) {
    this.query = query
    this.nextCursorMark = null
  }

  getCitations(nextPage = false) {
    const {
      query,
    } = this
    let ws = true
    let resultType = 'core'
    let cursorMark
    if (nextPage) {
      cursorMark = this.nextCursorMark
    } else {
      cursorMark = '*'
    }
    return fetchSearchResults({ query, ws, resultType, cursorMark })
      .then((response) => response.json())
      .then((obj) => {
        this.nextCursorMark = obj.nextCursorMark
        return {hitCount: obj.hitCount, citations: obj.resultList.result}
      })
  }
}