import Vue from 'vue'
import { defineStore } from "pinia"
import { BCT } from "@/api/bct"

const fullListStores = {}

export function getFullListStore(lid) {
    if (!(lid in fullListStores)) {
        fullListStores[lid] = defineStore(`fullListStore_${lid}`, {
            state: () => {
                return {
                    promise: null,
                    list: [],
                    lid: lid,
                    reranked: false,
                }
            },
            actions: {
                fetchList() {
                    if (this.promise === null) {
                        this.promise = BCT.getArticleList(lid)
                            .then(list => {
                                this.list = list
                                list.articles.sort((a, b) => {
                                    return a.rank - b.rank
                                })
                                if (!this.reranked) {
                                    this.reranked = true
                                    const ranks = list.articles.map(e => e.rank)
                                    for (let i = 0; i < ranks.length; i++) {
                                        if (i > 0) {
                                            if (ranks[i] === ranks[i-1]) {
                                                BCT.rerankArticleList(this.lid).then(() => this.fetchList())
                                                return
                                            }
                                        }
                                    }    
                                }
                                this.promise = null
                            })
                    }
                    return this
                }
            }
        })().fetchList()
    }
    return fullListStores[lid]
}

const listsByArticleStore = {}

export function getListsByArticleStore(source, externalId) {
    const key = `${source}${externalId}`
    if (!(key in listsByArticleStore)) {
        listsByArticleStore[key] = defineStore(`listsByArticleStore_${key}`, {
            state: () => {
                return {
                    lists: {},
                    key: key,
                    source: source,
                    externalId: externalId,
                    count: 0,
                    promise: null
                }
            },
            actions: {
                fetchLists() {
                    if (this.promise === null) {
                        this.promise = BCT.getArticleListsByArticles([{source, externalId}])
                            .then(elements => {
                                this.$patch((state) => {
                                    state.lists = {}
                                    state.count = elements.length
                                    for (const element of elements) {
                                        const { listId, source, externalId } = element
                                        if (source !== state.source || externalId !== state.externalId) {
                                            throw Error()
                                        }
                                        state.lists[listId] = true
                                    }
                                })
                                this.promise = null
                            })
                    }
                    return this
                }
            }
        })().fetchLists()
    }
    return listsByArticleStore[key]
}

export function getListsByArticles(articles) {
    BCT.getArticleListsByArticles(articles)
        .then(elements => {
            const listedArticles = new Map()
            for (const element of elements) {
                const { listId, source, externalId } = element
                const key = `${source}${externalId}`
                if (!(listedArticles.has(key))) {
                    listedArticles.set(key, {source, externalId, lists: {}})
                }
                listedArticles.get(key).lists[listId] = true
            }
            for (const [key, obj] of listedArticles) {
                const store = getListsByArticleStore(obj.source, obj.externalId)
                store.$patch({ lists: obj.lists })
            }
        })
    return Object.fromEntries(
        articles.map(ar => 
            [`${ar.source}${ar.externalId}`, getListsByArticleStore(ar.source, ar.externalId)]))
}


export const useSavedArticlesStore = defineStore('savedArticles', {
    state: () => {
        return {
            promise: null,
            lists: null,
        }
    },
    getters: {
        articleLists(state) {
            if (state.lists === null) {
                this.fetchLists()
            }
            return state.lists
        },
        articleListsByLid(state) {
            return new Map(state.lists.map(l => [l.id, l]))
        },
        quickLists(state) {
            if (state.articleLists === null) {
                return []
            }
            return state.articleLists.filter((list => {
                return list.details.quicklist
            }))
        }
    },
    actions: {
        fetchLists() {
            this.promise = BCT.getArticleLists()
                .then(lists => {
                    this.lists = lists
                    this.promise = null
                    return this.lists
                })
            return this.promise
        },
        createList(name, details) {
            return BCT.createArticleList(name, details)
                .then(() => {return this.fetchLists()})
        },
        deleteList(lid) {
            return BCT.deleteArticleList(lid)
                .then(() => {return this.fetchLists()})
        },
        updateList(lid, name, details) {
            return BCT.updateArticleList(lid, name, details)
                .then(() => {return this.fetchLists()})
        },
        moveListElement(lid, source, externalId, rank) {
            return BCT.updateArticleListElement(lid, source, externalId, rank, null)
                .then(() => {
                    return this.refreshLists(lid, source, externalId)
                })
        },
        deleteListElement(lid, source, externalId) {
            return BCT.deleteArticleListElement(lid, source, externalId)
                .then(() => {
                    return this.refreshLists(lid, source, externalId)
                })
        },
        createArticleListElement(lid, source, externalId, rank, details) {
            return BCT.createArticleListElement(...arguments)
                .then(() => {
                    return this.refreshLists(lid, source, externalId)
                })
        },
        maxRankForList(lid) {
            const list = this.articleListsByLid.get(lid)
            return Math.max(0, ...list.articles.map(a => a.rank))
        },
        rerankList(lid) {
            return BCT.rerankArticleList(lid)
                .then(() => {
                    getFullListStore(lid).fetchList()
                    return this.fetchLists()
                })
        },
        refreshLists(lid, source, externalId) {
            if (lid in fullListStores) {
                getFullListStore(lid).fetchList()
            }
            const key = `${source}${externalId}`
            if (key in listsByArticleStore) {
                getListsByArticleStore(source, externalId).fetchLists()
            }
            return this.fetchLists()
        }
    }
})