import { defineStore } from "pinia"
import { BCT } from "@/api/bct"

export const useSavedSearchesStore = defineStore('savedSearchs', {
    state: () => {
        return {
            searches: null,
            promise: null,
        }
    },
    getters: {
        savedSearches(state) {
            if (state.searches === null) {
                this.fetchSearches()
            }
            return state.searches || []
        }
    },
    actions: {
        fetchSearches() {
            this.promise = BCT.getSavedSearches()
                .then(searches => {
                    this.searches = searches
                    this.promise = null
                    return this.searches
                })
            return this.promise
        },
        createSavedSearch(parameters, name, details) {
            return BCT.createSavedSearch(parameters, name, details)
                .then(() => {return this.fetchSearches()})
        },
        deleteSavedSearch(id) {
            return BCT.deleteSavedSearch(id)
                .then(() => {return this.fetchSearches()})
        },
        updateSavedSearch(id, parameters, name, details) {
            return BCT.updateSavedSearch(id, parameters, name, details)
                .then(() => {return this.fetchSearches()})
        }
    }
})