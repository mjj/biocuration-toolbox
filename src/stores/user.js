import { defineStore } from "pinia"

export const useUserStore = defineStore('user', {
    state: () => {
        return {
            loggedIn: false,
            username: '',
            email: '',
        }
    }
})