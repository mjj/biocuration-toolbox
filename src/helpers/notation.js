export function tagDOM(root) {
    let walker = root.ownerDocument.createTreeWalker(root, NodeFilter.SHOW_ELEMENT)
    let visiting = walker.parentNode()
    const address = []
    OUTER_LOOP:
    while (true) {
        if (visiting = walker.firstChild()) {
            address.push(0)
            visiting.setAttribute('node-id', `${address.join('-')}`)
        } else if (visiting = walker.nextSibling()) {
            address.push(address.pop() + 1)
            visiting.setAttribute('node-id', `${address.join('-')}`)
        } else {
            INNER_LOOP:
            while (true) {
                if (visiting = walker.parentNode()) {
                    address.pop()
                    if (visiting = walker.nextSibling()) {
                        address.push(address.pop() + 1)
                        visiting.setAttribute('node-id', `${address.join('-')}`)
                        break INNER_LOOP
                    }
                } else {
                    break OUTER_LOOP
                }
            }
        }
    }

}

export function getRangeObj(range) {
    const startNodeType = range.startContainer.nodeType
    const endNodeType = range.endContainer.nodeType
    let startElement
    let endElement
    let startNode
    let endNode
    let rangeStartOffset
    let rangeEndOffset
    if (startNodeType === Node.CDATA_SECTION_NODE || 
        startNodeType === Node.COMMENT_NODE ||
        endNodeType === Node.CDATA_SECTION_NODE ||
        endNodeType === Node.COMMENT_NODE) {
        throw new Error("Invalid selection node type")
    }
    if (startNodeType !== Node.TEXT_NODE) {
        startElement = range.startContainer
        startNode = startElement.childNodes[range.startOffset]
        rangeStartOffset = 0
    } else {
        startElement = range.startContainer.parentElement
        startNode = range.startContainer
        rangeStartOffset = range.startOffset
    }
    if (endNodeType !== Node.TEXT_NODE) {
        endElement = range.endContainer
        endNode = endElement.childNodes[range.endOffset - 1]
        rangeEndOffset = endNode.textContent.length - 1
    } else {
        endElement = range.endContainer.parentElement
        endNode = range.endContainer
        rangeEndOffset = range.endOffset
    }
    while (startElement.nodeType !== Node.ELEMENT_NODE || !startElement.hasAttribute('node-id')) {
        startElement = startElement.parentElement
    }
    while (endElement.nodeType !== Node.ELEMENT_NODE || !endElement.hasAttribute('node-id')) {
        endElement = endElement.parentElement
    }
    const startID = startElement.getAttribute('node-id')
    const endID = endElement.getAttribute('node-id')
    const startOffset = getAnchorOffset(startNode, rangeStartOffset)
    const endOffset = getAnchorOffset(endNode, rangeEndOffset)
    return { startID, endID, startOffset, endOffset }
}

export function getAnchorOffset(node, nodeOffset) {
    let curNode = node
    let offset = nodeOffset
    while (!curNode.parentElement.hasAttribute('node-id')) {
        curNode = curNode.parentElement
    }
    while (curNode = curNode.previousSibling) {
        offset += curNode.textContent.length
    }
    return offset
}

export function getAnchorForOffset(parent, nodeOffset) {
    let walker = document.createTreeWalker(parent, NodeFilter.SHOW_TEXT)
    let visiting
    let curOffset = nodeOffset
    while (visiting = walker.nextNode()) {
        if (curOffset - visiting.length <= 0) {
            return [visiting, curOffset]
        }
        curOffset -= visiting.length
    }
    console.error('Ran out of nodes')
}
export function getRange(rangeObj) {
    const { startID, endID, startOffset, endOffset } = rangeObj
    const range = new Range()
    const startNode = document.querySelector(`[node-id='${startID}']`)
    const endNode = document.querySelector(`[node-id='${endID}']`)
    range.setStart(...getAnchorForOffset(startNode, startOffset))
    range.setEnd(...getAnchorForOffset(endNode, endOffset))
    return range
}

export function highlightRange(range, attributes) {
    const { startContainer, endContainer, startOffset, endOffset } = range
    let end
    let start
    if (endContainer.nodeType === Node.TEXT_NODE) {
        end = endContainer.splitText(endOffset)
    } else {
        end = endContainer
    }
    if (startContainer.nodeType === Node.TEXT_NODE) {
        start = startContainer.splitText(startOffset)
    } else {
        start = startContainer
    }
    let container = range.commonAncestorContainer
    let walker = document.createTreeWalker(container)
    let visiting
    let inHighlight = false
    let elements = []
    while (visiting = walker.nextNode()) {
        if (visiting === end) {
            break
        }
        if (visiting === start) {
            inHighlight = true
        }
        if ((visiting.nodeType === Node.TEXT_NODE) && inHighlight) {
            let newSpan = document.createElement('span')
            for (const [attr, value] of Object.entries(attributes)) {
                newSpan.setAttribute(attr, value)
            }
            visiting.parentNode.replaceChild(newSpan, visiting)
            newSpan.appendChild(visiting)
            elements.push(newSpan)
        }
    }
    return elements
}

export function canWrap(node) {
    return (window.getComputedStyle(node).display === "inline")
}

export function removeNotations(root, selectors) {
    let notations = root.querySelectorAll(selectors)
    let parents = new Set()
    for (const element of notations) {
        parents.add(element.parentElement)
        element.replaceWith(element.textContent)
    }
    for (const element of parents) {
        element.normalize()
    }
}

export function removeNotationsByClass(root, notationClass) {
    removeNotations(root, `[class*="${notationClass}"]`)
}

export function removeNotationsByNotationID(root, nid) {
    removeNotations(root,`[nid="${nid}"]`)
}

export function isSelectionForwards() {
    const selection = document.getSelection()
    const aspect = selection.anchorNode.compareDocumentPosition(selection.focusNode)
    if (aspect & Node.DOCUMENT_POSITION_FOLLOWING) return true
    if (aspect & Node.DOCUMENT_POSITION_PRECEDING) return false
    return selection.anchorOffset < selection.focusOffset
}