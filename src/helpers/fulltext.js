import { EPMC_ROOT } from "@/api/query"

function replaceText(text) {
    let newText = text
  
    // newText = newText.replace(/<(\w+)(.+?)\/>/g, '<$1$2></$1>')
    newText = newText.replace(/<!\[CDATA\[/g, '')
    newText = newText.replace(/]]>/g, '')
    newText = newText.replace('.pmc-wm', '.epmc-elements .pmc-wm')
    newText = newText.replace(/(">)PubMed(<\/a>])/g, '$1Abstract$2')
    newText = newText.replace(
      /(">)(PMC free article<\/a>]<\/span>)/g,
      '$1Europe $2'
    )
    newText = newText.replace(
      /<div><h2>(Contents<\/h2>)/g,
      '<div id="toc_id"><h2 id="toc_idtitle">$1'
    )
    newText = newText.replace(
      /(targettype=pubmed">)PubMed(<span class="bk_prnt">)/g,
      '$1Abstract$2'
    )
    newText = newText.replace(/(wm-retraction)(.png)/g, '$-pink$2')
  
    newText = newText.replace(/https:\/\/europepmc.org\//g, EPMC_ROOT)
    newText = newText.replace(
      new RegExp(EPMC_ROOT + 'abstract', 'g'),
      EPMC_ROOT + 'article'
    )
    newText = newText.replace(
      /"https:\/\/europepmc.org\/articles\/([^/]+)\/"/g,
      '"' + EPMC_ROOT + 'search?query=$1"'
    )
  
    newText = newText.replace(/(href=")[^//]\//g, '$1' + EPMC_ROOT)
    newText = newText.replace(/(src=")\//g, '$1' + EPMC_ROOT)
    newText = newText.replace(/(src=")(corehtml)/g, '$1' + EPMC_ROOT + '$2')
    newText = newText.replace(/(src=")\/(corehtml)/g, '$1' + EPMC_ROOT + '$2')
    newText = newText.replace(/(src=").\/(corehtml)/g, '$1' + EPMC_ROOT + '$2')
    newText = newText.replace(/(url\()(corehtml)/g, '$1' + EPMC_ROOT + '$2')
    newText = newText.replace(/(url\()\/(corehtml)/g, '$1' + EPMC_ROOT + '$2')
    newText = newText.replace(/(url\().\/(corehtml)/g, '$1' + EPMC_ROOT + '$2')
    newText = newText.replace(
      /(<a href=[^>]+)target="pmc_ext"([^>]+>Abstract<\/a>)/g,
      '$1$2'
    )
  
    return newText
  }
  
export function processFulltextDoc(fulltextId, response) {
    const doc = response.preprintDocument || response
    let fulltext = ''
    if (response.preprintDocument) {
      const tempDiv = document.createElement('div')
      tempDiv.appendChild(doc)
      fulltext = tempDiv.innerHTML
      // handle math equations on firefox
      fulltext = fulltext
        .replace(/&lt;math/g, '<math')
        .replace(/MathML"&gt;/g, 'MathML">')
        .replace(/&lt;\/math&gt;/g, '</math>')
  
      response.files.forEach((file) => {
        const { filename, type, mimeType } = file
  
        const linkFileNames = [filename]
        // whether needing the range of the id***
        if (
          fulltextId.startsWith('PPR') &&
          fulltextId >= 'PPR7001' &&
          fulltextId <= 'PPR7035' &&
          type === 'supplement'
        ) {
          const parts = filename.split('.')
          const fileExt = parts.pop()
          let newFileName = parts.join('.')
          const fid = newFileName.split('-').pop()
          newFileName = fulltextId + '-supplement-s' + ('00' + fid).slice(-3)
  
          linkFileNames[0] = newFileName + '.' + fileExt
          if (fileExt === 'xls') {
            linkFileNames[1] = newFileName + '.xlsx'
          }
        }
  
        linkFileNames.forEach((linkFileName) => {
          fulltext = fulltext.replace(
            new RegExp('"([^"]+)' + linkFileName + '"', 'g'),
            EPMC_API_FT_REPO_ROOT +
              (fulltextId.startsWith('PPR') ? 'pprId' : 'pmcId') +
              '=' +
              fulltextId +
              '&type=FILE&fileName=' +
              filename +
              '&mimeType=' +
              mimeType
          )
        })
      })
    } else if (['PMC', 'NBK', 'mid'].includes(fulltextId.substring(0, 3))) {
      if (['PMC', 'mid'].includes(fulltextId.substring(0, 3))) {
        // tempDiv used to update document head and fulltext; tempBody used for query
        const tempDiv = document.createElement('div')
  
        let tempBody = ''
        if (typeof doc === 'string') {
          tempBody = document.createElement('div')
          tempBody.innerHTML = replaceText(doc)
        } else if (typeof doc === 'object') {
          tempBody = doc
        }
  
        //Get blue sidebar
        const headStyle = tempBody.querySelector(
          'content-area[name="html-head-supplement"] > content'
        ).innerHTML
        tempDiv.innerHTML = replaceText(
          headStyle.substring(
            headStyle.indexOf('<style '),
            headStyle.indexOf('</style>')
          ) + '</style>'
        )
        document.head.appendChild(tempDiv.firstChild)
  
        tempDiv.innerHTML = replaceText(
          tempBody.querySelector('content-area[name="page-banner"] > content')
            .innerHTML
        )
        fulltext += tempDiv.innerHTML
  
        tempDiv.innerHTML = replaceText(
          tempBody.querySelector('content-area[name="page-body"] > content')
            .innerHTML
        )
        tempDiv.firstChild.className = ''
        fulltext += tempDiv.innerHTML
      } else if (fulltextId.substring(0, 3) === 'NBK') {
        if (doc.querySelector('div.pre-content')) {
          fulltext += doc.querySelector('div.pre-content').outerHTML
        }
        if (doc.querySelector('div.source-links')) {
          fulltext += doc.querySelector('div.source-links').outerHTML
        }
        if (doc.querySelector('div.main-content')) {
          fulltext += doc.querySelector('div.main-content').outerHTML
        }
        if (doc.querySelector('div.post-content')) {
          fulltext += doc.querySelector('div.post-content').outerHTML
        }
        fulltext = replaceText(fulltext)
      }
  
      // wrapper used to query and update fulltext
      const wrapper = document.createElement('div')
      wrapper.innerHTML = fulltext
  
      const citeLinks = wrapper.querySelectorAll('a[href$="citedby/"]')
      for (const link of citeLinks) {
        link.href = '#impact'
        link.innerHTML += '<i class="far fa-arrow-alt-circle-down"></i>'
        link.classList.add('openCitations')
      }
  
      const versionLinks = wrapper.querySelectorAll(
        '.fm-citation-ids a[class="int-reflink"]'
      )
      for (const link of versionLinks) {
        link.dataset.id = link.innerHTML
        link.dataset.source = link.href.indexOf('/mid/') >= 0 ? 'mid/' : ''
        link.classList.add('reloadFulltext')
      }
  
      const nextToggles = wrapper.querySelectorAll(
        '.meta-content .jig-ncbitoggler, .icnblk_cntnt .jig-ncbitoggler, a[href="#other_ver_id"]'
      )
      for (const toggle of nextToggles) {
        toggle.href = '#'
        toggle.classList.add('openNext')
        const i = document.createElement('i')
        i.setAttribute('class', 'fas fa-caret-right')
        toggle.appendChild(i)
      }
  
      fulltext = wrapper.innerHTML
    }
  
    return fulltext
  }
  