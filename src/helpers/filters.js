export const enumToVuetify = (en) => {
    return Object.entries(en).map((e, _) => { return { text: e[1], value: e[0] } })
}

export const arrayToDict = (ar) => {
    return Object.fromEntries(ar.map(e => [e.value, e]))
}

export const filterTypes = {
    entityType: 'entityTypes',
    entities: 'entities',
    term: 'term',
}

export const entitySections = [
    "Title",
    "Abstract",
    "Introduction",
    "Methods",
    "Results",
    "Discussion",
    "Acknowledgments",
    "References",
    "Table",
    "Figure",
    "Case study",
    "Supplementary material",
    "Conclusion",
    "Abbreviations",
    "Competing Interests",
]

export const entityFilterTypes = {
    include: 'include',
    exclude: 'exclude',
    ignore: 'ignore',
}

export const entityTypes = [
    {
        text: 'Accession Numbers',
        value: 'Accession Numbers',
        prompt: 'accession numbers',
        eg: 'A0A1B4WRL5',
        prefix: 'ACCESSION'
        // http://identifiers.org/ebi/ena.embl:OQ860748
    },
    {
        text: 'Gene Ontology',
        value: 'Gene Ontology',
        prompt: 'GO accessions',
        eg: 'GO:0009056'
        // http://identifiers.org/go/GO:0007601
    },
    {
        text: 'Genes/Proteins',
        value: 'Gene_Proteins',
        prompt: 'UniProt accessions',
        eg: 'A0A1B4WRL5',
        prefix: 'UNIPROT'
        // http://purl.uniprot.org/uniprot/P52183
    },
    {
        text: 'Diseases',
        value: 'Diseases',
        prompt: 'MeSH concept IDs',
        eg: 'C0013595',
        prefix: 'MESH'
        // http://linkedlifedata.com/resource/umls-concept/C0011860
    },
    {
        text: 'Organisms',
        value: 'Organisms',
        prompt: 'taxonomy identifiers',
        eg: '9685',
        prefix: 'TAXONOMY',
        // http://identifiers.org/taxonomy/702717
    },
    {
        text: 'Cell',
        value: 'Cell',
        prompt: 'cell ontology terms',
        eg: 'CL_0000236'
        // http://purl.obolibrary.org/obo/CL_1001474
    },
    {
        text: 'Cell Line',
        value: 'Cell Line',
        prompt: 'Cellosaurus accessions',
        eg: 'CVCL_1861'
        // http://web.expasy.org/cellosaurus/CVCL_E536
    },
    {
        text: 'Organ Tissue',
        value: 'Organ Tissue',
        prompt: 'uberon ontology terms',
        eg: 'UBERON_0001465'
        // http://purl.obolibrary.org/obo/UBERON_0000104
    },
    // {
    //     text: 'Anatomy',
    //     value: 'Anatomy',
    // },
    // {
    //     text: 'Phenotype',
    //     value: 'Phenotype',
    // },
    // {
    //     text: 'body-site',
    //     value: 'body-site',
    //     prompt: 'Experimental factor ontology',
    //     eg: 'EFO_1002012',
    // },
    // {
    //     text: 'Sequence',
    //     value: 'Sequence',
    //     prompt: 'Sequence Types and Features Ontology terms',
    //     eg: 'SO_0000417',
    // },
    // {
    //     text: 'Molecule',
    //     value: 'Molecule',
    //     prompt: 'SNOMED Clinical Terms',
    //     eg: '409893003'
    // },
    // {
    //     text: 'Molecular Process',
    //     value: 'Molecular Process',
    //     prompt: 'Molecular Process Ontology terms',
    //     eg: 'MOP_0000568'
    // },
    // {
    //     text: 'Transcription factor – Target gene',
    //     value: 'Transcription factor - Target gene',
    //     prompt: 'ENSEMBL ID',
    //     eg: 'ENSG00000133794'
    // },
    // {
    //     text: 'Pathway',
    //     value: 'Pathway',
    //     prompt: 'SNOMED Clinical Terms',
    //     eg: '74256009'
    // },
    // {
    //     text: 'Protein-protein Interactions',
    //     value: 'Protein Interaction',
    // },
    // {
    //     text: 'Phosphorylation Event',
    //     value: 'Biological Event',
    // },
    // {
    //     text: 'Genetic mutations',
    //     value: 'Gene Mutations',
    // },
    // {
    //     text: 'Gene-Disease OpenTargets',
    //     value: 'Gene Disease OpenTargets',
    // },
    // {
    //     text: 'Gene-Disease DisGeNET',
    //     value: 'Gene Disease DisGeNET',
    // },
    // -
    {
        text: 'Chemicals',
        value: 'Chemicals',
        prompt: 'ChEBI',
        eg: 'CHEBI:18248 identifiers'
        // http://purl.obolibrary.org/obo/CHEBI_10545
    },
    // {
    //     text: 'Clinical Drug',
    //     value: 'Clinical Drug',
    //     prompt: 'RxNORM identifiers',
    //     eg: '1114883'
    // },
    {
        text: 'Experimental Methods',
        value: 'Experimental Methods',
        prompt: 'Experimental Factor Ontology terms',
        eg: 'EFO:0000001'
        // http://www.ebi.ac.uk/efo/EFO_0004016
        // http://purl.obolibrary.org/obo/OBI_0000070
        // http://purl.obolibrary.org/obo/MP_0001845
    },
    {
        text: 'Resources',
        value: 'Resources',
        prompt: 'Identifiers.org identifiers',
        eg: 'MIR:00000350'
        // https://www.arb-silva.de
    },
    // {
    //     text: 'Sample-Material',
    //     value: 'Sample-Material',
    //     description: 'Sample from which the microbiome is extracted',
    // },
    // {
    //     text: 'Body-Site',
    //     value: 'Body-Site',
    //     description: "Host's body region/structure where microbiome is found",
    // },
    // {
    //     text: 'Host',
    //     value: 'Host',
    //     description: 'The organism where the microbiome is found',
    // },
    // {
    //     text: 'State',
    //     value: 'State',
    //     description: 'Host/Environment state',
    // },
    // {
    //     text: 'Site',
    //     value: 'Site',
    //     description: "Microbiome's site within place",
    // },
    // {
    //     text: 'Place',
    //     value: 'Place',
    //     description: "Microbiome's place or geocoordinates",
    // },
    // {
    //     text: 'Date',
    //     value: 'Date',
    //     description: 'Sampling date',
    // },
    // {
    //     text: 'Engineered',
    //     value: 'Engineered',
    //     description: "Microbiome's man-made environment",
    // },
    // {
    //     text: 'Ecoregion',
    //     value: 'Ecoregion',
    //     description: "Micrbiome's natural environment",
    // },
    // {
    //     text: 'Treatment',
    //     value: 'Treatment',
    //     description: 'Host/Environment treatments',
    // },
    // {
    //     text: 'Kit',
    //     value: 'Kit',
    //     description: 'Nucleic acid extraction-kit',
    // },
    // {
    //     text: 'Primer',
    //     value: 'Primer',
    //     description: 'PCR primers',
    // },
    // {
    //     text: 'Gene',
    //     value: 'Gene',
    //     description:
    //         'Target gene(s) (e.g. hypervariable regions of 16s/18s rRNA gene)',
    // },
    // {
    //     text: 'LS',
    //     value: 'LS',
    //     description: 'Library strategy (e.g amplicon, whole metagenome)',
    // },
    // {
    //     text: 'LCM',
    //     value: 'LCM',
    //     description: 'Library construction method (e.g. paired-end, single-end)',
    // },
    // {
    //     text: 'Sequencing',
    //     value: 'Sequencing',
    //     description: 'Sequencing platform',
    // },
    // {
    //     text: 'COVoc',
    //     value: 'COVoc',
    // },
]

export const filterDescriptions = {
    include: 'contains',
    exclude: 'does not contain',
    ignore: 'contains annotations other than'
}
